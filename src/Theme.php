<?php

namespace KevinKao\LiptonTheme;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

use Cache;
use Carbon\Carbon;

use \KevinKao\Lipton\Models\CmsCategory;
use \KevinKao\Lipton\Models\CmsPost;
use \KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Constants as LiptonConstants;
use \KevinKao\LiptonTheme\Constants;

class Theme
{
    const HALF_DAY = 60 * 60 * 12;
    const ONE_HOUR = 60 * 60;

    // 除了隱藏的類別
    protected $visibleCategories;
    // 排除隱藏, 外部連結, 純粹的文章類別
    protected $generalCategories;

    public function __construct()
    {
        $this->visibleCategories = CmsCategory::visible()->orderBy('order')->get();
        $this->generalCategories = CmsCategory::visible()
                ->where('type', LiptonConstants::CMS_CATEGORY_TYPE_GENERAL)
                ->orderBy('order')
                ->get();

        Collection::macro('separate', function ($size) {
            if ($size <= 0) {
                return new static;
            }
            $items = $this->items;

            $chunks = [];
            $chunks = new static;
            $chunks[] = array_slice($items, 0, $size);
            $chunks[] = array_slice($items, $size);
            return new static($chunks);
        });
    }

    public function library($path, $secure = null)
    {
        return asset(config('theme.library_path') . '/' . $path, $secure);
    }

    public function noImg()
    {
        return asset('/img/noimg.png');
    }

    public function categories()
    {
        return $this->visibleCategories;
    }

    public function onlyPostCategories()
    {
        return $this->generalCategories;
    }

    public function parentCategories($offset = 0, $limit = 10)
    {
        return CmsCategory::visible()
            ->where('parent_id', null)
            ->offset($offset)
            ->limit($limit)
            ->orderBy('order')
            ->get();
    }

    public function posts($limit = 15, $order = Constants::ORDER_DESC)
    {
        $query = CmsPost::whereIn('cms_category_id', $this->generalCategories->pluck('id'))->take($limit);
        return $order === Constants::ORDER_ASC ?
            $query->orderBy('created_at')->get() :
            $query->orderByDesc('created_at')->get();
    }

    public function postsByRange($offset, $limit = 15, $order = Constants::ORDER_DESC)
    {
        return CmsPost::whereIn('cms_category_id', $this->generalCategories->pluck('id'))
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', $order)
            ->get();
    }

    // 隨機取得指定日期內的文章(預設1個月內)
    public function postsByDateRange($limit = 15, $from = null, $to = null)
    {
        if ($from == null) {
            $from = date('Y-m-d', strtotime('-1 month'));
        }

        if ($to == null) {
            $to = date('Y-m-d', strtotime('+1 day'));
        }
        return Cache::remember("postsByDateRange:{$limit}:{$from}:{$to}", self::HALF_DAY, function() use ($limit, $from, $to) {
            return CmsPost::whereIn('cms_category_id', $this->generalCategories->pluck('id'))
                ->whereBetween('created_at', [$from, $to])
                ->inRandomOrder()
                ->take($limit)
                ->get();
        });
    }

    // 隨機取得分類指定日期內的文章(預設1個月內)
    public function categoryPostsByDateRange($categoryId, $limit = 15, $from = null, $to = null)
    {
        if ($from == null) {
            $from = date('Y-m-d', strtotime('-1 month'));
        }
        if ($to == null) {
            $to = date('Y-m-d', strtotime('+1 day'));
        }
        return Cache::remember("categoryPostsByDateRange:{$categoryId}:{$limit}:{$from}:{$to}", self::HALF_DAY, function() use ($categoryId, $limit, $from, $to) {
            return CmsPost::whereIn('cms_category_id', [$categoryId])
                ->whereBetween('created_at', [$from, $to])
                ->inRandomOrder()
                ->take($limit)
                ->get();
        });
    }

    public function postsByRandomToday($limit = 15)
    {
        return Cache::remember("postsByRandomToday:{$limit}", Carbon::tomorrow()->diffInSeconds(Carbon::now()), function () use ($limit) {
            return CmsPost::whereIn('cms_category_id', $this->generalCategories->pluck('id'))->inRandomOrder()->take($limit)->get();
        });
    }

    public function postsByRandom($limit = 15)
    {
        return Cache::remember("postsByRandom:{$limit}", self::ONE_HOUR, function() use ($limit) {
            return CmsPost::whereIn('cms_category_id', $this->generalCategories->pluck('id'))
                ->inRandomOrder()
                ->take($limit)
                ->get();
        });
    }

    public function postsByCategory($categoryId, $limit = 15, $order = Constants::ORDER_DESC)
    {
        $category = CmsCategory::find($categoryId);
        if (empty($category)) {
            return collect([]);
        }
        $query = $category->posts()->take($limit);
        return $order === Constants::ORDER_ASC ?
            $query->orderBy('created_at')->get() :
            $query->orderByDesc('created_at')->get();
    }

    public function postsByCategoryOrder($categoryId, $limit = 15, $order = Constants::ORDER_DESC)
    {
        $category = CmsCategory::find($categoryId);
        if (empty($category)) {
            return collect([]);
        }
        $query = $category->posts()->take($limit);
        return $order === Constants::ORDER_ASC ?
            $query->orderBy('order')->get() :
            $query->orderByDesc('order')->get();
    }

    public function randomInCategory($categoryId, $limit = 15)
    {
        return Cache::remember("randomInCategory:{$categoryId}:{$limit}", self::ONE_HOUR, function() use ($categoryId, $limit) {
            return CmsPost::whereIn('cms_category_id', [$categoryId])->inRandomOrder()->take($limit)->get();
        });
    }

    /**
     * 隨機取得一個可見的分類並取得底下的文章
     * @param integer $limit
     * @param string $order
     * @return Collection
     */
    public function randomCategoryWithPosts($limit = 15, $order = Constants::ORDER_DESC)
    {
        return Cache::remember("randomCategoryWithPosts:{$limit}:{$order}", self::ONE_HOUR, function() use ($limit, $order) {
            $category = CmsCategory::visible()->inRandomOrder()->first();
            return $category->posts()->orderBy('created_at', $order)->take($limit)->get();
        });
    }

    public function postsByCategorySlug($slug, $limit = 15, $order = Constants::ORDER_DESC)
    {
        $category = CmsCategory::where('slug', $slug)->first();
        if (empty($category)) {
            return collect([]);
        }
        $query = $category->posts()->take($limit);
        return $order === Constants::ORDER_ASC ?
            $query->orderBy('created_at')->get() :
            $query->orderByDesc('created_at')->get();
    }
    
    /**
     * 依照分類指定排序來取得底下文章
     * 通常用在直接使用順序不用再用slug來取
     * @param  integer $sequence 需要取得的順位
     * @param  integer $limit
     * @param  integer $order
     * @return Collection
     */
    public function postsByCategorySequence($sequence, $limit = 15, $order = Constants::ORDER_DESC)
    {
        if (!is_int($sequence)) {
            throw new \Exception('$sequence must be an integer.', 1);
        }
        $categories = CmsCategory::visible()
            ->whereIn('id', $this->generalCategories->pluck('id'))
            ->orderBy('order')
            ->get();

        if (!isset($categories[$sequence])) {
            return collect([]);
        }

        return $categories[$sequence]->posts()
                ->when($order === Constants::ORDER_DESC, function($query) {
                    $query->orderByDesc('created_at');
                })
                ->when($order === Constants::ORDER_ASC, function($query) {
                    $query->orderBy('created_at');
                })
                ->take($limit)
                ->get();
    }

    /**
     * 使用指定slug分類隨機取得底下的文章
     * @param  string  $slug
     * @param  integer $limit
     * @return Collection
     */
    public function postsByCategoryRandom($slug, $limit = 15)
    {
        return Cache::remember("postsByCategoryRandom:{$slug}:{$limit}", self::ONE_HOUR, function() use ($slug, $limit) {
            $category = CmsCategory::where('slug', $slug)->first();
            if (empty($category)) {
                return collect([]);
            }
            return $category->posts()->inRandomOrder()->take($limit)->get();
        });
    }

    /**
     * 使用post來取得該post上一篇文章
     * @param  \KevinKao\Lipton\Models\CmsPost $post
     * @return \KevinKao\Lipton\Models\CmsPost
     */
    public function prevPost($post)
    {
        return CmsPost::where('cms_category_id', $post->cms_category_id)
            ->where('id', '<', $post->id)
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * 使用post來取得該post下一篇文章
     * @param  \KevinKao\Lipton\Models\CmsPost $post
     * @return \KevinKao\Lipton\Models\CmsPost
     */
    public function nextPost($post)
    {
        return CmsPost::where('cms_category_id', $post->cms_category_id)
            ->where('id', '>', $post->id)
            ->orderBy('id', 'asc')
            ->first();
    }

    public function randomMediaImage($limit = 15)
    {
        return Cache::remember("randomMediaImage:{$limit}", self::ONE_HOUR, function() use ($limit) {
            return CmsMedia::where('type', LiptonConstants::CMS_MEDIA_TYPE_IMAGE)
                ->whereNotNull('post_id')
                ->inRandomOrder()
                ->take($limit)
                ->get();
        });
    }
}