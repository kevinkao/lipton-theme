<?php

namespace KevinKao\LiptonTheme\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Http\Request;

use KevinKao\Lipton\Models\CmsPost;
use Theme;

class PostComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $post;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->post = CmsPost::find($request->id);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('post', $this->post);
        $view->with('prevPost', Theme::prevPost($this->post));
        $view->with('nextPost', Theme::nextPost($this->post));
        $view->with('categoryTitle', !empty($this->post->category) ? $this->post->category->title : '');
        $view->with('categoryLink', !empty($this->post->category) ? route('category', $this->post->cms_category_id) : '#');
    }
}