<?php

namespace KevinKao\LiptonTheme\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Http\Request;

use KevinKao\Lipton\Models\CmsCategory;

class CategoryComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $category;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->category = CmsCategory::find($request->id);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'category' => $this->category,
            'posts' => $this->category->posts()->orderBy('created_at', 'desc')->paginate()
        ]);

    }
}