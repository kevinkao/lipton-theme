<?php

namespace KevinKao\LiptonTheme\Http\Middleware;

use Closure;
use \KevinKao\Lipton\Models\CmsPost;

class CheckPostExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        CmsPost::findOrFail($request->id);
        return $next($request);
    }
}
