<?php

namespace KevinKao\LiptonTheme\Http\Middleware;

use Closure;
use \KevinKao\Lipton\Models\CmsCategory;

class CheckCategoryExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        CmsCategory::findOrFail($request->id);
        return $next($request);
    }
}
