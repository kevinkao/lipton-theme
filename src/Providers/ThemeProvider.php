<?php

namespace KevinKao\LiptonTheme\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;

use KevinKao\LiptonTheme\Theme;
use KevinKao\LiptonTheme\Http\Middleware\CheckCategoryExists;
use KevinKao\LiptonTheme\Http\Middleware\CheckPostExists;

class ThemeProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('check.category.exists', CheckCategoryExists::class);
        $router->aliasMiddleware('check.post.exists', CheckPostExists::class);
        if (config('theme.apply_theme_route') && file_exists(base_path("routes/theme.php"))) {
            // $this->loadRoutesFrom(base_path("routes/theme.php"));
            Route::middleware('web')->group(base_path("routes/theme.php"));
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('Theme', \KevinKao\LiptonTheme\Facades\Theme::class);
        $this->app->singleton('theme', function () {
            return new Theme();
        });

        if ($this->app->runningInConsole()) {
            $dir = dirname(__DIR__);

            $this->publishes([
                "{$dir}/../publishes/controllers/" => app_path('Http/Controllers/'),
                "{$dir}/../publishes/config/theme.php" => config_path("theme.php"),
                "{$dir}/../publishes/routes/theme.php" => base_path("routes/theme.php"),
                "{$dir}/../publishes/views/" => resource_path("views/"),
                "{$dir}/../publishes/library/" => public_path('library'),
                "{$dir}/../publishes/lang/" => resource_path('lang'),
                "{$dir}/../publishes/factories/" => database_path('factories'),
                "{$dir}/../publishes/seeds/" => database_path('seeds'),
                "{$dir}/../publishes/img/" => public_path('img'),
            ]);
        }
    }
}