<?php

namespace KevinKao\LiptonTheme\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use KevinKao\Lipton\Models\CmsCategory;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['theme.desktop.detail', 'theme.mobile.detail'],
            'KevinKao\LiptonTheme\Http\View\Composers\PostComposer'
        );
        View::composer(
            ['theme.desktop.category', 'theme.mobile.category'],
            'KevinKao\LiptonTheme\Http\View\Composers\CategoryComposer'
        );
    }
}