<?php

namespace KevinKao\LiptonTheme;

class Constants
{
    const ORDER_ASC          = 'asc';
    const ORDER_DESC         = 'desc';

    const SLUG_NEWS          = 'news';
    const SLUG_BEAUTY        = 'beauty';
    const SLUG_FICTION       = 'fiction';
    const SLUG_ADS           = 'ads';
    const SLUG_SITES         = "sites";
    const SLUG_ENTERTAINMENT = 'entertainment'; // 娛樂新聞
}