<?php

Route::group(['namespace' => '\App\Http\Controllers\Theme'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/category/{id}', 'CategoryController@index')->middleware(['check.category.exists'])->name('category');
    Route::get('/detail/{id}', 'DetailController@index')->middleware(['check.post.exists'])->name('detail');
    Route::get('/page/{view}', 'PageController@index')->name('page');

    Route::get('/m', 'HomeController@mobile')->name('m.home');
    Route::get('/m/category/{id}', 'CategoryController@mobile')->middleware(['check.category.exists'])->name('m.category');
    Route::get('/m/detail/{id}', 'DetailController@mobile')->middleware(['check.post.exists'])->name('m.detail');
    Route::get('/m/page/{view}', 'PageController@mobile')->name('m.page');

    Route::post('register', "AuthController@register")->name('register');
    Route::post('login', "AuthController@login")->name('login');
    Route::post('logout', "AuthController@logout")->name('logout');
});