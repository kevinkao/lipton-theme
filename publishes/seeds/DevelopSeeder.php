<?php

use Illuminate\Database\Seeder;

use \KevinKao\Lipton\Models\CmsCategory;
use \KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Models\CmsPost;

class DevelopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CmsCategory::class, 6)
            ->create()
            ->each(function($category) {
                factory(CmsPost::class, 30)
                ->create()
                ->each(function($post) use ($category) {
                    $media = factory(CmsMedia::class)->create();
                    $post->cms_category_id = $category->id;
                    $post->cover_id = $media->id;
                    $post->save();
                });
            });
    }
}
