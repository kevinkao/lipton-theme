<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class DetailController extends Controller
{
    public function index($id)
    {
        if (config('theme.detect_mobile')) {
            $agent = new Agent();
            if ($agent->isMobile()) {
                return redirect()->route('m.detail');
            }
        }
        return view('theme.desktop.detail');
    }

    public function mobile($id)
    {
        return view('theme.mobile.detail');
    }
}

