<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class PageController extends Controller
{
    public function index(Request $request, $view)
    {
        if (config('theme.detect_mobile')) {
            $agent = new Agent();
            if ($agent->isMobile()) {
                return redirect()->route('m.page');
            }
        }
        return view("theme.desktop.{$view}");
    }

    public function mobile(Request $request, $view)
    {
        return view("theme.mobile.{$view}");
    }
}
