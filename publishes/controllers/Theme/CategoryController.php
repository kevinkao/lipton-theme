<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class CategoryController extends Controller
{
    public function index($id)
    {
        if (config('theme.detect_mobile')) {
            $agent = new Agent();
            if ($agent->isMobile()) {
                return redirect()->route('m.category');
            }
        }
        return view('theme.desktop.category');
    }

    public function mobile($id)
    {
        return view('theme.mobile.category');
    }
}
