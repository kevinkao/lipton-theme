<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use KevinKao\Lipton\Errors;
use KevinKao\Lipton\Models\Role;
use KevinKao\Lipton\Models\User;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email|unique:KevinKao\Lipton\Models\User,email',
                    'password' => 'required|confirmed',
                    'name' => 'required',
                    'phone_number' => 'required',
                    'captcha' => 'required|captcha',
                ],
                __('validation'),
                [
                    'email' => '邮箱',
                    'password' => '密码',
                    'name' => '名称',
                    'phone_number' => '手机号',
                    'captcha' => '验证码',
                ]
            );

            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), Errors::VALIDATION_FAILED);
            }

            $userRole = Role::where('name', 'user')->first();

            DB::beginTransaction();
            $user = new User();
            $user->email = htmlspecialchars($request->input('email'));
            $user->password = bcrypt($request->input('password'));
            $user->name = htmlspecialchars(strip_tags($request->input('name')));
            $user->phone_number = strip_tags($request->input('phone_number', null));
            // $user->qq = htmlspecialchars($request->input('qq', null));
            $user->save();
            $user->roles()->attach($userRole->id);
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'code' => 0,
                    'data' => $user
                ]);
            } else {
                return redirect()->route('login')->with('success', '注册成功');
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                if ($e->getCode() === Errors::VALIDATION_FAILED) {
                    return response()->json([
                        'code' => Errors::VALIDATION_FAILED,
                        'message' => $e->getMessage()
                    ], 422);
                }
            } else {
                return redirect()->back()->withErrors([
                    'msg' => $e->getMessage(),
                    'type-register' => true
                ]);
            }

            Log::error($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                    'message' => $e->getMessage()
                ], 500);
            } else {
                return redirect()->back()->withErrors([
                    'msg' => $e->getMessage(),
                    'type-register' => true
                ]);
            }
        }
    }

    public function login(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required|string',
                ],
                __('validation'),
                [
                    'email' => '邮箱',
                    'password' => '密码',
                ]
            );

            if ($validator->fails()) {
                throw new Exception($validator->errors()->first(), Errors::VALIDATION_FAILED);
            }

            if (Auth::attempt([
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ])) {
                if ($request->ajax()) {
                    return response()->json([
                        'message' => 'Login success.',
                    ]);
                } else {
                    return redirect()->route('home');
                }
            } else {
                throw new Exception('邮箱或密码错误', Errors::VALIDATION_FAILED);
            }
        } catch (Exception $e) {
            if ($e->getCode() === Errors::VALIDATION_FAILED) {
                if ($request->ajax()) {
                    return response()->json([
                        'code' => Errors::VALIDATION_FAILED,
                        'message' => $e->getMessage()
                    ], 422);
                } else {
                    return redirect()->back()->withErrors([
                        'msg' => $e->getMessage(),
                        'type-login' => true
                    ]);
                }
            }

            Log::error($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                    'message' => $e->getMessage()
                ], 500);
            } else {
                return redirect()->back()->withErrors([
                    'msg' => $e->getMessage(),
                    'type' => 'login'
                ]);
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        if ($request->ajax()) {
            return response()->json([
                'message' => 'Logout success.',
            ]);    
        } else {
            return redirect()->back();
        }
    }
}
