<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class HomeController extends Controller
{
    public function index()
    {
        if (config('theme.detect_mobile')) {
            $agent = new Agent();
            if ($agent->isMobile()) {
                return redirect()->route('m.home');
            }
        }
        return view('theme.desktop.home');
    }

    public function mobile()
    {
        return view('theme.mobile.home');
    }
}
