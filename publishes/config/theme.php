<?php

return [
    /**
     * 偵測手機自動切換手機版
     * 如果是RWD就改成false, 不要偵測
     */
    'detect_mobile' => false,
    /**
     * 是否採用套件提供的route
     * 如果採用會自動載入routes/theme.php
     */
    'apply_theme_route' => true,
    /**
     * 第三方套件路徑
     */
    'library_path' => '/library',
];