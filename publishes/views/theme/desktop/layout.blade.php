<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ settings('site_title') }}</title>
    <meta name="description" content="{{ settings('site_description') }}" />
    <meta name="keywords" content="{{ settings('site_keyword') }}" />
    <link rel="stylesheet" href="{{ Theme::library('/bootstrap/4.5.3/bootstrap.min.css') }}">
    @stack('styles')
</head>
<body>
    @yield('container')

    <script src="{{ Theme::library('/jquery/jquery-3.5.1.slim.min.js') }}"></script>
    <script src="{{ Theme::library('/bootstrap/4.5.3/bootstrap.bundle.min.js') }}"></script>
    @stack('scripts')
    {!! settings('site_analysis_code') !!}
</body>
</html>