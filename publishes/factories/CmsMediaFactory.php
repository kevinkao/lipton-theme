<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use \KevinKao\Lipton\Models\CmsMedia;

$factory->define(CmsMedia::class, function (Faker $faker) {
    return [
        'path' => $faker->imageUrl(random_int(400, 600), random_int(200,400))
    ];
});
