<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use \KevinKao\Lipton\Models\CmsPost;

$factory->define(CmsPost::class, function (Faker $faker) {
    return [
        'author_id' => 1,
        'title' => $faker->sentence(3),
        // 'content' => implode('', $faker->paragraphs(3))
        'content' => $faker->randomHtml()
    ];
});
