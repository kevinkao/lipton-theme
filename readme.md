## Installation

Update composer.json like below example first.
``` 
"license": "MIT",
"type": "project",
"repositories": [{
    "type": "vcs",
    "url": "https://gitlab.com/kevinkao/lipton-theme.git"
}],
```

And then, update composer packages.

```
$ composer require kevinkao/lipton-theme
```

```
$ php artisan vendor:publish
 Which provider or tag's files would you like to publish?:
  [0 ] Publish files from all providers and tags listed below
  [1 ] Provider: Fideloper\Proxy\TrustedProxyServiceProvider
  [2 ] Provider: Illuminate\Foundation\Providers\FoundationServiceProvider
  [3 ] Provider: Illuminate\Mail\MailServiceProvider
  [4 ] Provider: Illuminate\Notifications\NotificationServiceProvider
  [5 ] Provider: Illuminate\Pagination\PaginationServiceProvider
  [6 ] Provider: KevinKao\LiptonFeedback\Providers\FeedbackProvider
  [7 ] Provider: KevinKao\LiptonTheme\Providers\ThemeProvider
  [8 ] Provider: KevinKao\Lipton\Providers\LiptonServiceProvider
  [9 ] Provider: Laravel\Tinker\TinkerServiceProvider
  [10] Tag: laravel-errors
  [11] Tag: laravel-mail
  [12] Tag: laravel-notifications
  [13] Tag: laravel-pagination
  [14] Tag: lipton_assets
  [15] Tag: lipton_config
  [16] Tag: lipton_seeds
 > 7
```

## 剛clone新的專案時, 需安裝相依套件
```
composer install
```

## 更新最新套件
```
composer update kevinkao/lipton-theme
```

成功後
```
php artisan vendor:publish
```

```
 Which provider or tag's files would you like to publish?:
  [0 ] Publish files from all providers and tags listed below
  [1 ] Provider: Facade\Ignition\IgnitionServiceProvider
  [2 ] Provider: Fideloper\Proxy\TrustedProxyServiceProvider
  [3 ] Provider: Illuminate\Foundation\Providers\FoundationServiceProvider
  [4 ] Provider: Illuminate\Mail\MailServiceProvider
  [5 ] Provider: Illuminate\Notifications\NotificationServiceProvider
  [6 ] Provider: Illuminate\Pagination\PaginationServiceProvider
  [7 ] Provider: KevinKao\LiptonTheme\Providers\ThemeProvider
  [8 ] Provider: KevinKao\Lipton\Providers\LiptonServiceProvider
  [9 ] Provider: Laravel\Tinker\TinkerServiceProvider
  [10] Tag: flare-config
  [11] Tag: ignition-config
  [12] Tag: laravel-errors
  [13] Tag: laravel-mail
  [14] Tag: laravel-notifications
  [15] Tag: laravel-pagination
  [16] Tag: lipton_assets
  [17] Tag: lipton_config
  [18] Tag: lipton_seeds
 > 7 
 ```

選擇 [7 ] Provider: KevinKao\LiptonTheme\Providers\ThemeProvider

```
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/controllers] To [/app/Http/Controllers]
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/views] To [/resources/views]
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/library] To [/public/library]
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/lang] To [/resources/lang]
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/factories] To [/database/factories]
Copied Directory [/vendor/kevinkao/lipton-theme/publishes/seeds] To [/database/seeds]
```

再來更新composer清單
```
composer dumpautoload
```

到這邊應該套件是新版了, 如果要重置整個資料庫方便開發

# 重置資料庫內容
```
php artisan migrate:fresh
```
# 新增站點基本資料
```
php artisan db:seed --class LiptonSeeder
```
# 新增測試資料
```
php artisan db:seed --class DevelopSeeder
```